package com.gmail.vvatsak;

import java.util.Date;

public class Cat {
	private String color;
	private double weight;
	private Date birthday;
	private boolean sleep = true;

	public Cat(String color, double weight, Date birthday, boolean sleep) {
		super();
		this.color = color;
		this.weight = weight;
		this.birthday = birthday;
		this.sleep = sleep;
	}

	public Cat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public boolean isSleep() {
		return sleep;
	}

	public void kisKis() {
		sleep = false;
	}

	@Override
	public String toString() {
		return "Cat [color=" + color + ", weight=" + weight + " Kg, birthday=" + birthday + ", sleep=" + sleep + "]";
	}

}
