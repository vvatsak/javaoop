package com.gmail.vvatsak;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SimpleDateFormat sdf = new SimpleDateFormat("dd:MM:yyyy HH:mm");
		String dateText = "25:07:2010 22:30";
		Date date = new Date();

		try {
			date = sdf.parse(dateText);
		} catch (ParseException e) {
			System.out.println(e);
		}
		Cat tom = new Cat("grey", 15.5, date, true);
		System.out.println(tom);
		tom.kisKis();
		System.out.println(sdf.format(tom.getBirthday()));
		Cat markiz = new Cat();
		System.out.println(markiz);
		System.out.println(tom.getWeight());
		System.out.println(tom.getColor());
	}
}
