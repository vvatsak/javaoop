package com.gmail.vvatsak;

import java.util.Arrays;

public class Group {
	private Student[] stdGrp = new Student[10];

	public Group() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void setStdToGrp(Student student) {
		try {
			boolean has = false;
			for (int i = 0; i < stdGrp.length; i++) {
				if (stdGrp[i] == student) {

					has = true;
					break;
				}
			}

			if (has == false) {

				for (int i = 0; i < stdGrp.length; i++) {
					if (stdGrp[i] == null) {
						stdGrp[i] = student;
						System.out.println("Student " + student.getLastName() + " included in group");
						return;

					}
				}
				throw new OutOfIndexArrayException();
			}

			if (has == true) {
				System.out.println("Student " + student.getLastName() + " alredy in group");
			}
		} catch (OutOfIndexArrayException e) {
			System.out.println(e);
		}
	}

	public void delStdFromGrp(Student student) {
		boolean out = true;
		for (int i = 0; i < stdGrp.length; i++) {
			if (stdGrp[i] == student) {
				stdGrp[i] = null;
				out = false;
			}
		}
		System.out.println(out ? "Student " + student.getLastName() + " hasn't in group" : "Student excluded");
	}

	public void srchStdInGrp(String a) {
		boolean inGrp = false;
		for (int i = 0; i < stdGrp.length; i++) {
			if (stdGrp[i] == null) {
				continue;
			} else if (stdGrp[i].getLastName() == a) {
				System.out.println("Student " + a + " has position " + i + " in group");
				inGrp = true;
			}
		}
		if (inGrp == false)
			System.out.println("Student " + a + " hasn't in group");
	}

	@Override
	public String toString() {
		return "Group [stdGrp=" + Arrays.toString(stdGrp) + "]";
	}

	public String toStrings() {
		StringBuffer sb = new StringBuffer();
		String[] arr = new String[stdGrp.length];
		try {
			for (int i = 0; i < stdGrp.length; i++) {
				arr[i] = (stdGrp[i].getLastName() + ", " + stdGrp[i].getFirstName() + ", " + stdGrp[i].isMan() + ", "
						+ stdGrp[i].getBirthday());
			}

			for (int j = 0; j < arr.length; j++) {

				for (int i = j + 1; i < arr.length; i++) {

					if (arr[i].compareToIgnoreCase(arr[j]) < 0) {

						String t = arr[j];

						arr[j] = arr[i];

						arr[i] = t;
					}
				}
				sb.append(arr[j] + "\n");
			}
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		return sb.toString();
	}
}
