package com.gmail.vvatsak;

import java.util.Date;

public class Human {
	private String firstName;
	private String lastName;
	private boolean isMan;
	private Date birthday;

	public Human() {
		super();
		// TODO Auto-generated constructor stub

	}

	public Human(String firstName, String lastName, boolean isMan, Date birthday) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.isMan = isMan;
		this.birthday = birthday;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public boolean isMan() {
		return isMan;
	}

	public void setMen(boolean isMan) {
		this.isMan = isMan;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {
		return "Human [firstName=" + firstName + ", lastName=" + lastName + ", isMan=" + isMan + ", birthday="
				+ birthday + "]";
	}
}
