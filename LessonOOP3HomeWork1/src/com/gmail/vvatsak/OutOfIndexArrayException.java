package com.gmail.vvatsak;

public class OutOfIndexArrayException extends Exception {
	@Override
	public String getMessage() {
		return "Group is full";
	}
}
