package com.gmail.vvatsak;

import java.util.Date;

public class Main {

	public static void main(String[] args) {
		Human anton = new Human("Anton", "Topolev", true, new Date(925145688554L));
		System.out.println(anton);
		Student igor = new Student("Igor", "Karpov", true, new Date(825145688554L));
		Student ivan = new Student("Ivan", "Savin", true, new Date(895145688554L));
		Student olga = new Student("Olga", "Krin", false, new Date(745145680554L));
		Student oleg = new Student("Oleg", "Potapov", true, new Date(695145688554L));
		Student petro = new Student("Petro", "Pirogov", true, new Date(655145688554L));
		Student marina = new Student("Marina", "Kalinina", false, new Date(875145688554L));
		Student stanislav = new Student("Stanislav", "Lopatin", true, new Date(775145688554L));
		Student maxim = new Student("Maxim", "Dybov", true, new Date(755175698504L));
		Student elena = new Student("Elena", "Loseva", false, new Date(865472698504L));
		Student taras = new Student("Taras", "Klenov", true, new Date(765078699504L));
		Student galina = new Student("Galina", "Parysova", false, new Date(795078694504L));
		System.out.println(igor);
		Group grpOne = new Group();
		Group grpTwo = new Group();
		grpOne.setStdToGrp(igor);
		grpOne.setStdToGrp(igor);
		grpOne.setStdToGrp(ivan);
		grpOne.setStdToGrp(olga);
		grpOne.setStdToGrp(oleg);
		grpOne.setStdToGrp(petro);
		grpOne.setStdToGrp(marina);
		grpOne.setStdToGrp(stanislav);
		grpOne.setStdToGrp(maxim);
		grpOne.setStdToGrp(elena);
		grpOne.setStdToGrp(taras);
		grpOne.setStdToGrp(galina);

		grpOne.srchStdInGrp("Karpov");
		grpOne.srchStdInGrp("Krin");
		grpOne.srchStdInGrp("Test");
		System.out.println(grpOne.toStrings());
		grpTwo.setStdToGrp(maxim);
		grpTwo.setStdToGrp(elena);
		grpTwo.setStdToGrp(taras);
		System.out.println(grpTwo.toStrings());
	}
}
