package com.gmail.vvatsak;

import java.util.Date;

public class Student extends Human {

	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Student(String firstName, String lastName, boolean isMan, Date birthday) {
		super(firstName, lastName, isMan, birthday);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Student [getFirstName()=" + getFirstName() + ", getLastName()=" + getLastName() + ", isMan()=" + isMan()
				+ ", getBirthday()=" + getBirthday() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}

}
