package com.gmail.vvatsak;

public class Quadrate extends Shape {
	private Point a = new Point();
	private Point b = new Point();

	public Quadrate() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Quadrate(Point a, Point b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	double getPerimetr() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		return ab * 4;
	}

	@Override
	double getArea() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		return Math.pow(ab, 2);
	}

	@Override
	public String toString() {
		return "Quadrate [a=" + a + ", b=" + b + "]";
	}

}
