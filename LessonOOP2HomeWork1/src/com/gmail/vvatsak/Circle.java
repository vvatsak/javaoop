package com.gmail.vvatsak;

public class Circle extends Shape {
	private Point a = new Point();
	private Point b = new Point();

	public Circle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Circle(Point a, Point b) {
		super();
		this.a = a;
		this.b = b;
	}

	@Override
	double getPerimetr() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		return 2 * ab * Math.PI;
	}

	@Override
	double getArea() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		return ab * ab * Math.PI;
	}

	@Override
	public String toString() {
		return "Circle [a=" + a + ", b=" + b + "]";
	}

}
