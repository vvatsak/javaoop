package com.gmail.vvatsak;

public class Triangle extends Shape {
	private Point a = new Point();
	private Point b = new Point();
	private Point c = new Point();

	public Triangle() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Triangle(Point a, Point b, Point c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	@Override
	double getPerimetr() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2) + Math.pow((b.getY() - c.getY()), 2));
		double ac = Math.sqrt(Math.pow((a.getX() - c.getX()), 2) + Math.pow((a.getY() - c.getY()), 2));
		double p = (ab + bc + ac);
		return p;
	}

	@Override
	double getArea() {
		double ab = Math.sqrt(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2));
		double bc = Math.sqrt(Math.pow((b.getX() - c.getX()), 2) + Math.pow((b.getY() - c.getY()), 2));
		double ac = Math.sqrt(Math.pow((a.getX() - c.getX()), 2) + Math.pow((a.getY() - c.getY()), 2));
		double p = (ab + bc + ac) / 2;
		double s = Math.sqrt((p - ab) * (p - bc) * (p - ac) * p);
		return s;
	}

	@Override
	public String toString() {
		return "Triangle [a=" + a + ", b=" + b + ", c=" + c + "]";
	}

}
