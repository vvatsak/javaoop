package com.gmail.vvatsak;

import java.util.Arrays;

public class Board {
	private Shape[] a = new Shape[4];

	public Board() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void addPlaceOne(Shape b) {
		if (a[0] != null) {
			System.out.println("Place One busy!");
		} else
			this.a[0] = b;

	}

	public void delPlaceOne() {
		this.a[0] = null;

	}

	public void addPlaceTwo(Shape b) {
		if (a[1] != null) {
			System.out.println("Place Two busy!");
		} else
			this.a[1] = b;

	}

	public void delPlaceTwo() {
		this.a[1] = null;

	}

	public void addPlaceThree(Shape b) {
		if (a[2] != null) {
			System.out.println("Place Three busy!");
		} else
			this.a[2] = b;

	}

	public void delPlaceThree() {
		this.a[2] = null;

	}

	public void addPlaceFour(Shape b) {
		if (a[3] != null) {
			System.out.println("Place Four busy!");
		} else
			this.a[3] = b;

	}

	public void delPlaceFour() {
		this.a[3] = null;

	}

	public void getAllPlaces() {
		double s = 0;
		for (int i = 0; i < a.length; i += 1) {
			System.out.println("Place " + (i + 1) + " " + a[i]);
			if (a[i] != null) {
				s = s + a[i].getPerimetr();
			}
		}
		System.out.println("Perimetr Shapes on Board = " + s);
	}

	@Override
	public String toString() {
		return "Board a=" + Arrays.toString(a);
	}

}