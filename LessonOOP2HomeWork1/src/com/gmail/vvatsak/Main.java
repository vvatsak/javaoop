package com.gmail.vvatsak;

public class Main {
	public static void main(String[] args) {
		Point a = new Point(4, 1);
		System.out.println(a);
		Triangle b = new Triangle(new Point(1, 6), new Point(2, -100), new Point(4, 2));
		System.out.println(b);
		System.out.println(b.getPerimetr());
		System.out.println(b.getArea());
		Quadrate c = new Quadrate(new Point(4, -2), new Point(7, 0));
		System.out.println(c);
		System.out.println(c.getPerimetr());
		System.out.println(c.getArea());
		Circle d = new Circle(new Point(3, 1), new Point(4, 2));
		System.out.println(d);
		System.out.println(d.getPerimetr());
		System.out.println(d.getArea());
		Board e = new Board();
		e.addPlaceOne(new Circle(new Point(3, 1), new Point(4, 2)));
		e.addPlaceThree(new Quadrate(new Point(4, -2), new Point(7, 0)));
		e.delPlaceOne();
		e.addPlaceThree(new Circle(new Point(-8, 0.7), new Point(9, 14)));
		e.addPlaceTwo(new Triangle(new Point(1, 6), new Point(2, -100), new Point(4, 2)));
		e.addPlaceOne(new Quadrate(new Point(5.2, 0.7), new Point(6, 3.4)));
		e.getAllPlaces();
	}
}